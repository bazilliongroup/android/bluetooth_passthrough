package com.example.bluetooth_passthrough;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.util.Set;

public class MainActivity extends AppCompatActivity
{
    public static final int REQUEST_ENABLE_BT   = 0;

    public static final int STATE_CONNECTED             = 1;
    public static final int STATE_CONNECTION_FAILED     = 2;
    public static final int STATE_CONNECTION_TIMEOUT    = 3;
    public static final int STATE_DISCONNECTED          = 4;

    //storage for connected status.
    public static String mdeviceConnected;

    private Button mViewDevices;
    private Button mTurnOnBT;
    private Button mTurnOffBT;
    private Button mUserBtnRemote;
    private Button mBluetoothChat;
    private TextView mConnectionStatus;
    private ListView mListBondedPairs;

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothDevice[] mBtArray;

    // Create a handler for this Activity.
    private Handler mUserInterfaceHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mdeviceConnected = "DISCONNECTED";

        //Initialize buttons IDs to code declarations
        mViewDevices        = findViewById(R.id.Btn_ViewDevicesId);
        mTurnOnBT           = findViewById(R.id.Btn_TurnOnBT);
        mTurnOffBT          = findViewById(R.id.Btn_TurnOffBT);
        mUserBtnRemote = findViewById(R.id.Btn_ConfigureItemsId);
        mBluetoothChat = findViewById(R.id.Btn_BtChat);
        mConnectionStatus   = findViewById(R.id.TxtV_ConnectStatusId);
        mListBondedPairs    = findViewById(R.id.ListV_ListBondedPairsId);

        //Set BT adapter
        mBluetoothAdapter   = BluetoothAdapter.getDefaultAdapter();

        /*NOTE: The main user interface activity already has a message queue to process
        work from other threads, so all you need to do is setup the handler.

        If you want to setup an handler for another thread, you must add in it's own
        handler, looper and message queue to process messages coming to the thread.
        To do this, you must add in Looper.Prepare() before the Handler and Looper.loop()
        after the instantiation of handler which starts the infinite message loop.*/

        //Instantiating the handler in this class means this handler for this thread.
        mUserInterfaceHandler = new UserInterfaceHandler(mConnectionStatus);

        mConnectionStatus.setText(mdeviceConnected);

        //Check if bluetooth is available or not
        if (mBluetoothAdapter == null) { showToast("Bluetooth is not available"); }
        else { showToast("Bluetooth is available"); }

        //click listeners
        viewDevices();
        turnOnBT();
        turnOffBT();
        userBtnRemote();
        bluetoothChat();
        connectToDevice();
    }

    //toast message function
    private void showToast(String msg)
    {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    //click listener for View Devices Button
    private void viewDevices()
    {
        mViewDevices.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (mBluetoothAdapter.isEnabled())
                {
                    Set<BluetoothDevice> bt = mBluetoothAdapter.getBondedDevices();
                    String[] strings = new String[bt.size()];
                    mBtArray = new BluetoothDevice[bt.size()];
                    int index = 0;

                    if (bt.size() > 0)
                    {
                        for (BluetoothDevice device : bt)
                        {
                            mBtArray[index] = device;
                            strings[index] = device.getName();
                            index++;
                        }

                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, strings);
                        mListBondedPairs.setAdapter(arrayAdapter);
                    }
                    showToast("Viewing Bonded Devices");
                }
                else
                {
                    showToast("Must enable Bluetooth");
                }
            }
        });
    }

    //click listener for Turn On BT Button
    private void turnOnBT()
    {
        mTurnOnBT.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (!mBluetoothAdapter.isEnabled())
                {
                    showToast("Turning On Bluetooth...");
                    //Intent to turn on Bluetooth
                    Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(intent, REQUEST_ENABLE_BT);
                }
                else
                {
                    showToast("Bluetooth is already on");
                }
            }
        });
    }

    //click listener for Turn Off Button
    private void turnOffBT()
    {
        mTurnOffBT.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (mBluetoothAdapter.isEnabled())
                {
                    mBluetoothAdapter.disable();
                    Message message = Message.obtain();
                    message.what = STATE_DISCONNECTED;
                    mUserInterfaceHandler.sendMessage(message);
                } else
                {
                    showToast("Bluetooth is already off");
                }
            }
        });
    }

    //click listener for User Remote Button
    private void userBtnRemote()
    {
        mUserBtnRemote.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showToast("User Button Remote Button");
            }
        });
    }

    //click listener for Bluetooth Chat Button
    private void bluetoothChat()
    {
        mBluetoothChat.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showToast("Bluetooth Chat Button");
            }
        });
    }

    //List view On Item click Listener
    private void connectToDevice()
    {
        mListBondedPairs.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                if (mBluetoothAdapter.isEnabled())
                {
                    showToast("device clicked");
                    ClientClass clientClass = new ClientClass(mBtArray[i]);
                    clientClass.start();

                    mdeviceConnected = "CONNECTING...";
                    mConnectionStatus.setText(mdeviceConnected);
                }
                else
                {
                    showToast("Must enable Bluetooth");
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        if (requestCode == REQUEST_ENABLE_BT)
        {
            if (resultCode == RESULT_OK)
            {
                //bluetooth is on
                showToast("Bluetooth is on");
            } else
            {
                //user denied to turn on bluetooth
                showToast("could not turn on Bluetooth");
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private class ClientClass extends Thread
    {
        private BluetoothSocket mSocket;

        ClientClass( BluetoothDevice device)
        {

            try
            {
                mSocket = device.createRfcommSocketToServiceRecord(java.util.UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
            }
            catch(Exception e)
            {
                e.printStackTrace();
                Message message = Message.obtain();
                message.what = STATE_CONNECTION_FAILED;
                mUserInterfaceHandler.sendMessage(message);
            }
        }

        @Override
        public void run()
        {
            try
            {
                mSocket.connect();
                Message message = Message.obtain();
                message.what = STATE_CONNECTED;
                mUserInterfaceHandler.sendMessage(message);
            } catch (IOException e)
            {
                e.printStackTrace();
                Message message = Message.obtain();
                message.what = STATE_CONNECTION_FAILED;
                mUserInterfaceHandler.sendMessage(message);
            }
        }
    }

    private static class UserInterfaceHandler extends Handler
    {
        TextView mTextView;

        UserInterfaceHandler(TextView textView)
        {
            mTextView = textView;
        }

        @Override
        public void handleMessage(@NonNull Message msg)
        {
            switch(msg.what)
            {
                case STATE_CONNECTED:
                    mdeviceConnected = "CONNECTED";
                    mTextView.setText(mdeviceConnected);
                    break;
                case STATE_CONNECTION_FAILED:
                    mdeviceConnected = "CONNECTION FAILED";
                    mTextView.setText(mdeviceConnected);
                    break;
                case STATE_DISCONNECTED:
                    mdeviceConnected = "DISCONNECTED";
                    mTextView.setText(mdeviceConnected);
                    break;
                case STATE_CONNECTION_TIMEOUT:
                    mdeviceConnected = "SOCKET TIMEOUT";
                    mTextView.setText(mdeviceConnected);
                    break;
            }
        }
    }
}
